﻿namespace PL0RT
{
    public enum Fct
    {
        Lit, Opr, Lod, Sto, Cal, Inc, Jmp, Jpc
    }
}