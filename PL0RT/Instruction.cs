﻿namespace PL0RT
{
    public record Instruction
    {
        public Fct F; // OpCode
        public int L; // Level
        public int A; // Address
    }
}