﻿/***
 * Beim Übertragen von DELPHI nach C# erste einmal die Variablen so übernehmen, wie im DELPHI-Code.
 * Wenn die Funktionen der Bibliothek soweit funktionieren, dann den Variablen bessere Namen geben.
 */
using System;
using System.Collections.Generic;

namespace PL0RT
{
    public class Pl0Rt
    {
#region Globale Deklarationen
        public static List<Instruction> _codeSeg = new List<Instruction>(1);
        private static int Ip { get; set; } // Instruction pointer
        private static int Bp { get; set; } // Base pointer
        private static int Sp { get; set; } // Stack pointer
        private static Instruction Instruction { get; set; } // actual instruction
        private static int Stacksize { get; set; } = 512;
        public static int[] StackSeg { get; set; } = new int[Stacksize];
#endregion
        public void Interpret()
        {
            Sp = 0;
            Bp = 1;
            Ip = 0;
            do
            {
                Instruction = _codeSeg[Ip];
                Ip++;
                switch (Instruction.F)
                {
                    case Fct.Lit:
                        Sp += 1;
                        StackSeg[Sp] = Instruction.A;
                        break;
                    case Fct.Opr:
                        switch (Instruction.A) // operator
                        {
                            case 0:
                                Sp = Bp--;
                                Ip = StackSeg[Sp + 3];
                                Bp = StackSeg[Sp + 2];
                                break;
                            case 1:
                                StackSeg[Sp] = -StackSeg[Sp];
                                break;
                            case 2:
                                Sp--;
                                StackSeg[Sp] = StackSeg[Sp] + StackSeg[Sp + 1];
                                break;
                            case 3:
                                Sp--;
                                StackSeg[Sp] = StackSeg[Sp] - StackSeg[Sp + 1];
                                break;
                            case 4:
                                Sp--;
                                StackSeg[Sp] = StackSeg[Sp] * StackSeg[Sp + 1];
                                break;
                            case 5:
                                Sp--;
                                StackSeg[Sp] = StackSeg[Sp] / StackSeg[Sp + 1];
                                break;
                            case 6:
                                StackSeg[Sp] = Convert.ToInt32(IsOdd(StackSeg[Sp]));
                                break;
                            case 7: // No Operation (NOP)
                                break;
                            case 8:
                                Sp--;
                                StackSeg[Sp] = Convert.ToInt32(StackSeg[Sp] == StackSeg[Sp + 1]);
                                break;
                            case 9:
                                Sp--;
                                StackSeg[Sp] = Convert.ToInt32(StackSeg[Sp] != StackSeg[Sp + 1]);
                                break;
                            case 10:
                                Sp--;
                                StackSeg[Sp] = Convert.ToInt32(StackSeg[Sp] < StackSeg[Sp + 1]);
                                break;
                            case 11:
                                Sp--;
                                StackSeg[Sp] = Convert.ToInt32(StackSeg[Sp] >= StackSeg[Sp + 1]);
                                break;
                            case 12:
                                Sp--;
                                StackSeg[Sp] = Convert.ToInt32(StackSeg[Sp] > StackSeg[Sp + 1]);
                                break;
                            case 13:
                                Sp--;
                                StackSeg[Sp] = Convert.ToInt32(StackSeg[Sp] <= StackSeg[Sp + 1]);
                                break;
                            default:
                                throw new Exception("Illegal operation subcode!");
                        }
                        break;
                    case Fct.Lod:
                        Sp += 1;
                        StackSeg[Sp] = StackSeg[Base(Instruction.L) + Instruction.A];
                        break;
                    case Fct.Sto:
                        StackSeg[Base(Instruction.L) + Instruction.A] = StackSeg[Sp];
                        Console.Error.WriteLine($"{StackSeg[Sp]}");
                        Sp -= 1;
                        break;
                    case Fct.Cal:
                        // generate a new block mark
                        StackSeg[Sp + 1] = Base(Instruction.L);
                        StackSeg[Sp + 2] = Bp;
                        StackSeg[Sp + 3] = Ip;
                        Bp = Sp++;
                        Ip = Instruction.A;
                        break;
                    case Fct.Inc:
                        Sp += Instruction.A;
                        break;
                    case Fct.Jmp:
                        Ip = Instruction.A;
                        break;
                    case Fct.Jpc:
                        if (StackSeg[Sp] == 0)
                        {
                            Ip = Instruction.A;
                            Sp -= 1;
                        }
                        break;
                    default:
                        throw new Exception("Illegal instruction or opcode!");
                }
            } while (Ip != 0);
        }

        public bool IsOdd(int i) => i % 2 != 0;

        public void Gen(string[] s)
        {
            Instruction instruction = new Instruction();
            instruction.F = GetFct(s[0]);
            instruction.L = Int32.Parse(s[1]);
            instruction.A = Int32.Parse(s[2]);
            _codeSeg.Add(instruction);
        }

        public Fct GetFct(string s)
        {
            var opFct = Fct.Lit;
          
            switch (s.ToLower())
            {
                case "lit":
                case "0":
                    opFct = Fct.Lit;
                    break;
                case "opr":
                case "1":
                    opFct = Fct.Opr;
                    break;
                case "lod":
                case "2":
                    opFct = Fct.Lod;
                    break;
                case "sto":
                case "3":
                    opFct = Fct.Sto;
                    break;
                case "cal":
                case "4":
                    opFct = Fct.Cal;
                    break;
                case "int": // Aus Grunden der Kompatibilität und Historie der Entwicklung wird dieser OpCode noch interpretiert
                case "ink": // Der neue OpCode, der in den Pascal und PL/0-Quellen besser zu lesen und zu verstehen ist
                case "inc":
                case "5":
                    opFct = Fct.Inc;
                    break;
                case "jmp":
                case "6":
                    opFct = Fct.Jmp;
                    break;
                case "jpc":
                case "7":
                    opFct = Fct.Jpc;
                    break;
                default:
                    throw new Exception($"Not a valid opcode {opFct}!");
            }
            return opFct;
        }

        private int Base(int l)
        {
            int b1;
            b1 = Bp;
            while (l > 0)
            {
                b1 = StackSeg[b1];
                l--;
            }

            return b1;
        }
    }
}
