﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace PL0RT.Tests
{
    [TestClass()]
    public class Pl0RtTests
    {
        //Global Arrangieren
        private readonly Pl0Rt pl0Rt = new Pl0Rt();

        [DataTestMethod()]
        [DataRow("jmp 0 1;int 0 6;lit 0 10;sto 0 3;lit 0 20;sto 0 4;lod 0 3;lod 0 4;opr 0 4;sto 0 5;opr 0 0", 200)]
        [DataRow("6 0 1;5 0 6;0 0 10;3 0 3;0 0 20;3 0 4;2 0 3;2 0 4;1 0 4;3 0 5;1 0 0", 200)]
        public void InterpretTest(string s, int erwartet)
        {
            // Arrangieren
            var pl0code = s.Split(';');
            // Ausführen
            foreach (var instruction in pl0code)
            {
                pl0Rt.Gen(instruction.Split(' '));
            }
            pl0Rt.Interpret();
            // Feststellen
            Assert.AreEqual(erwartet, Pl0Rt.StackSeg[6]);
        }

        [DataTestMethod()]
        [DataRow(1, 1)]
        [DataRow(2, 0)]
        public void IsOddTest(int i, int erwartet)
        {
            // Arrangieren
            Int32 aktuell;
            // Ausführen
            aktuell = Convert.ToInt32(pl0Rt.IsOdd(i));
            // Feststellen
            Assert.AreEqual(erwartet, aktuell);
        }

        [DataTestMethod()]
        [DataRow("jmp 0 1", 0, Fct.Jmp, 0, 1)]
        [DataRow("opr 0 0", 1, Fct.Opr, 0, 0)]
        [DataRow("6 0 1", 2, Fct.Jmp, 0, 1)]
        [DataRow("1 0 0", 3, Fct.Opr, 0, 0)]
        public void GenTest(string s, int idx, Fct fct, int lvl, int addr)
        {
            // Arrangieren
            List<Instruction> CodeSegErwartet = new List<Instruction>(1);
            Instruction instruction = new Instruction();
            // Ausführen
            instruction.F = fct; instruction.L = lvl; instruction.A = addr;
            CodeSegErwartet.Add(instruction);
            pl0Rt.Gen(s.Split(' '));
            // Feststellen
            Assert.AreEqual(CodeSegErwartet[0], Pl0Rt._codeSeg[idx]);
        }

        [DataTestMethod()]
        [DataRow("jmp",Fct.Jmp)]
        [DataRow("opr", Fct.Opr)]
        [DataRow("6", Fct.Jmp)]
        [DataRow("1",Fct.Opr)]
        public void GetFctTest(string s, Fct fct)
        {
            // Arrangieren          
            Fct fctErwartet,
                fctAktuell;
            // Ausführen
            fctErwartet = fct;
            fctAktuell = pl0Rt.GetFct(s);
            // Feststellen
            Assert.AreEqual(fctErwartet, fctAktuell);
        }

        [DataTestMethod()]
        [DataRow("jnz 0 1")]
        [DataRow("42 0 1")]
        public void GenNotAValidOpCodeTest(string s)
        {
            // Arrangieren
            // Ausführen
            var ex = Assert.ThrowsException<Exception>(() => pl0Rt.Gen(s.Split(' ')));
            // Feststellen
            Assert.AreEqual("Not a valid opcode Lit!", ex.Message);
        }

        [DataTestMethod()]
        [DataRow("jmp 0 1;int 0 6;lit 0 10;sto 0 3;lit 0 20;sto 0 4;lod 0 3;lod 0 4;opr 0 42;sto 0 5;opr 0 0")]
        [DataRow("6 0 1;5 0 6;0 0 10;3 0 3;0 0 20;3 0 4;2 0 3;2 0 4;1 0 42;3 0 5;1 0 0")]
        public void InterpretIllegalOperationSubCodeTest(string s)
        {
            // Arrangieren
            var pl0code = s.Split(';');
            // Ausführen
            foreach (var instruction in pl0code)
            {
                pl0Rt.Gen(instruction.Split(' '));
            }
            var ex = Assert.ThrowsException<Exception>(() => pl0Rt.Interpret());
            // Feststellen
            Assert.AreEqual("Illegal operation subcode!", ex.Message);
        }

        [DataTestMethod()]
        [DataRow("jmp 0 1;int 0 6;lit 0 10;sto 0 3;lit 0 20;sto 0 4;lod 0 3;lod 0 4;opr 0 42;sto 0 5;opr 0 0", 42, 0, 1)]
        [DataRow("6 0 1;5 0 6;0 0 10;3 0 3;0 0 20;3 0 4;2 0 3;2 0 4;1 0 42;3 0 5;1 0 0", 42, 0, 1)]
        public void InterpretIllegalOpCodeTest(string s, int illFct, int lvl, int addr)
        {
            // Arrangieren
            var pl0code = s.Split(';');
            Instruction illegalInstruction = new Instruction();
            // Ausführen
            foreach (var instruction in pl0code)
            {
                pl0Rt.Gen(instruction.Split(' '));
            }
            illegalInstruction.F = (Fct)illFct; illegalInstruction.L = lvl; illegalInstruction.A = addr;
            Pl0Rt._codeSeg[1] = illegalInstruction;
            var ex = Assert.ThrowsException<Exception>(() => pl0Rt.Interpret());
            // Feststellen
            Assert.AreEqual("Illegal instruction or opcode!", ex.Message);
        }
    }
}