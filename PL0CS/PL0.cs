﻿using System;
using System.IO;
using PL0RT;
using static System.Environment;


namespace PL0
{
    class Pl0
    {
        private static string _readLine;
        private static readonly Pl0Rt Pl0Rt = new Pl0Rt();
        static void Main(string[] args)
        {
            try
            {
                using var reader = new StreamReader(args[0]);
                Console.SetIn(reader);
                while ((_readLine = Console.ReadLine()) != null) Pl0Rt.Gen(_readLine.Split(' '));
                Pl0Rt.Interpret();
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
                ExitCode = 1;
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e);
                ExitCode = 2;
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine(e);
                ExitCode = 3;
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
                ExitCode = 4;
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine(e);
                ExitCode = 5;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                ExitCode = 64;
            }

            ExitCode = 0;
        }
    }
}
