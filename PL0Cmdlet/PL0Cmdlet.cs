﻿using System;
using System.IO;
using System.Management.Automation;
using PL0RT;

namespace PL0Cmdlet
{
    [Cmdlet(VerbsLifecycle.Start,"PL0", DefaultParameterSetName = "Assembler")]
    public class Pl0Cmdlet : PSCmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Assembler", HelpMessage = "Einen Dateinamen mit PL/0 Assembler- oder OpCode-Anweisungen eingeben.")]
        [ValidateNotNullOrEmpty]
        public string AFile { get; set; } = String.Empty;

        private static string _readLine;
        private static readonly Pl0Rt Pl0Rt = new Pl0Rt();

        protected override void ProcessRecord()
        {
            try
            {
                using var reader = new StreamReader(AFile);
                while ((_readLine = reader.ReadLine()) != null) Pl0Rt.Gen(_readLine.Split(' '));
                Pl0Rt.Interpret();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
